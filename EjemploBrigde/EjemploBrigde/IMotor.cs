﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploBrigde
{
        public interface IMotor
        {
            void InyectarCombustible(double cantidad);
            void ConsumirCombustible();
        }

}